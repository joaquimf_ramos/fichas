/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.ficha1;

import java.util.Scanner;

/**
 *
 * @author jfvtr
 */
public class Ex3 {public static void main (String args[]) {

        // Exercicio 3
        
        Scanner ler = new Scanner(System.in);
        int anos, meses, dias, idadeDias;

        System.out.println("Introduza o numero de anos.");
        anos = ler.nextInt();
        System.out.println("Introduza o numero de meses.");
        meses = ler.nextInt();
        System.out.println("Introduza o numero de dias.");
        dias = ler.nextInt();

        idadeDias = anos*365 + meses*30 + dias;

        System.out.println("A idade em dias é:" + idadeDias);
     
    }


}