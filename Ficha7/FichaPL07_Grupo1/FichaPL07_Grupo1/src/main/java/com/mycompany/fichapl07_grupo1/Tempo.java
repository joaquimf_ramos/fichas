/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.fichapl07_grupo1;

/**
 *
 * @author Andre Mendes
 */
public class Tempo {
    private int hora = 0;
    private int minuto = 0;
    private int segundo = 0;
    
    public Tempo() {}
    
    public Tempo(int hora, int minuto, int segundo) {
        this.hora = hora;
        this.minuto = minuto;
        this.segundo = segundo;
    }
    
    
    String getTempo() {
        String resultado = "";
        
        if (hora < 10)
            resultado = "0" + hora;
        else
            resultado = "" + hora;
        
        resultado += ":";
        
        if (minuto < 10)
            resultado += "0" + minuto;
        else
            resultado += "" + minuto;
        
        resultado += ":";
        
        if (segundo < 10)
            resultado += "0" + segundo;
        else
            resultado += "" + segundo;
        
        return resultado;
    }
    
    String getTempoAMPM() {
        String resultado = "";
        int horaAux = hora; // 0 = 12; >12 = hora - 12
        String postfixo = ""; // AM/PM
        //0-11 AM
        //12-23 > PM
        //h = 0 > h = 12
        
        if (hora < 12)
            postfixo = "AM";
        else
            postfixo = "PM";
        
        // operador ternário
        // cond ? valor se V : valor se F
        // postfixo = hora < 12 ? "AM": "PM";
        
        if (hora == 0)
            horaAux = 12;
        else
            if (hora > 12)
                horaAux = hora - 12;

        
        if (horaAux < 10)
            resultado = "0" + horaAux;
        else
            resultado = "" + horaAux;
        
        resultado += ":";
        
        if (minuto < 10)
            resultado += "0" + minuto;
        else
            resultado += "" + minuto;
        
        resultado += ":";
        
        if (segundo < 10)
            resultado += "0" + segundo;
        else
            resultado += "" + segundo;
        
        resultado += " " + postfixo;
        
        return resultado;
    }
    
    void acrescentarSegundo() {
        segundo++;
        
        if (segundo == 60) {
            minuto++;
            segundo = 0;
        }
        
        if (minuto == 60) {
            hora++;
            minuto = 0;
        }
        
        if (hora == 24)
            hora = 0;
    }
    
    public static boolean eMaiorQue(Tempo t1, Tempo t2) {
        boolean resultado = false;
        
        if (t1.hora > t2.hora)
            resultado = true;
        
        if (t1.hora == t2.hora && t1.minuto > t2.minuto)
            resultado = true;
        
        if (t1.hora == t2.hora 
                && t1.minuto == t2.minuto
                && t1.segundo > t2.segundo
                )
            resultado = true;
        
        return resultado;
    }
    
    /**
     * Retorna verdadeiro se t for maior
     * @param t Tempo a comparar
     * @return 
     */
    public boolean eMaiorQue(Tempo t) {
        return eMaiorQue(this, t);
    }
    
    public boolean eMaiorQue(int hora, int minuto, int segundo) {
        Tempo t = new Tempo(hora, minuto, segundo);
        return eMaiorQue(this, t);
        
        /*
        return eMaiorQue(this, new Tempo(hora, minuto, segundo));
        */
    }
    
    
}
