/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.fichapl07_grupo1;

/**
 *
 * @author Andre Mendes
 */
public class TesteTempo {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // a)
        Tempo t1 = new Tempo(5, 30, 12);
        
        // b)
        System.out.println(t1.getTempoAMPM());
        
        // c)
        t1.acrescentarSegundo();
        
        // d)
        System.out.println(t1.getTempoAMPM());
        
        // e)
        Tempo t2 = new Tempo(18, 5, 20);
        
        // f)
        System.out.println(t2.getTempoAMPM());
        
        // g)
        if (t1.eMaiorQue(t2))
            System.out.println("t1 é maior que t2");
        else
            System.out.println("t1 não é maior que t2");
        
        /*
        if (Tempo.eMaiorQue(t1, t2))
            System.out.println("t1 é maior que t2");
        else
            System.out.println("t1 não é maior que t2");
        */
        
        // h)
        if (t1.eMaiorQue(23, 7, 4))
            System.out.println("t1 é maior que t2");
        else
            System.out.println("t1 não é maior que t2");
        

    }
    
}
