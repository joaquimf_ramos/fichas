/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.fichapl07_grupo2;

/**
 *
 * @author Andre Mendes
 */
public class Utente {
    private String nome;
    private String genero;
    private int idade;
    private double altura;
    private double peso;
    
    public Utente(String nome, String genero, 
            int idade, double altura, double peso)
    {
        this.nome = nome;
        this.genero = genero;
        this.idade = idade;
        this.altura = altura;
        this.peso = peso;
    }
    
    String getNome() {
        return nome;
    }
    
    void setNome(String nome) {
        this.nome = nome;
    }    
    
    String descricao() {
        String resultado = "";
        
        resultado =
                "O(a) " + nome 
                + " é do género " + genero
                + ", tem " + idade + " anos"
                + ", mede " + altura + " metros"
                + " e pesa " + peso + "kg.";
        
        return resultado;
    }
    
    
}
